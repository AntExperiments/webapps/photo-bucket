import Image from 'next/image'
import { useState } from 'react';
import style from './imageCard.module.css'

const ImageCard = ({ image }) => { 
    const [ modalIsOpen, setIsOpen ] = useState(false);
    const url = `/uploads/${image.filename}`

    const closePopup = ({ target }) =>
        target.className.includes("modalContainer") && setIsOpen(false)

    return (<>

        <a>
            <Image src={`${url}.thumb.png`} height="250px" width="300px" objectFit="cover" onClick={() => setIsOpen(true)} />
        </a>

        {modalIsOpen && 
        <div className={style.modalContainer} onClick={closePopup}>
            <div className={style.modal}>
                {!url.endsWith('.mp4')
                    && <img className={style.modalImage} src={url} />
                    || <video className={style.modalImage} controls autoPlay={true}>
                           <source src={url}/>
                       </video>
                }
            </div>
        </div>}
    </>)
}

export default ImageCard