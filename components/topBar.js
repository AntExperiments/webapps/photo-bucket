import styles from './topBar.module.css'
import Dropzone from 'react-dropzone'

const Uploader = ({ selectedTags }) => {
    const processFiles = files => {
        const body = new FormData();
    
        body.append('tags', JSON.stringify(selectedTags))
        files.forEach(file => {
            body.append(file.name, file)
        })
    
        fetch("/api/upload", {
            method: "POST",
            body
        })
        .then(_ => window.location.reload())
    }

    return (
    <Dropzone onDrop={processFiles}>
        {({getRootProps, getInputProps}) => (
            <section className={styles.Dropzone}>
                <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <span>Upload files...</span>
                </div>
            </section>
        )}
    </Dropzone>
)}

export default function TopBar ({ tags, updateTags }) {
    const toggleTag = name => {
        const newTags = JSON.parse( JSON.stringify(tags) )
        
        newTags.forEach(tag => {
            if (tag.name == name)
            tag.selected = !tag.selected
        })

        updateTags(newTags)
    }

    return (
        <div className={styles.root}>
            <div className={styles.tags}>
                {tags.map(tag => <a key={tag.name + tag.selected} className={tag.selected ? styles.selected : ""} onClick={() => toggleTag(tag.name)}>{tag.name}</a>)}
            </div>
            <Uploader selectedTags={tags.filter(t => t.selected).map(t => t.name)}/>
        </div>
    )
}