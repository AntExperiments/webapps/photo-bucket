import Formidable from "formidable-serverless";
import fs from "fs";
import genThumbnail from "simple-thumbnail";

export const config = {
  api: {
    bodyParser: false,
  },
};

export default function uploadFormFiles(req, res) {
  return new Promise(async (resolve, reject) => {
    const form = new Formidable.IncomingForm({
      multiples: true,
      keepExtensions: true,
    });

    let tags = []

    form
      .on("field", (name, value) => {
        tags = JSON.parse(value)
        if (tags.length < 1)
          tags = ["Unsorted"]
      })
      .on("file", (name, file) => {
        // write image
        const data = fs.readFileSync(file.path)
        fs.writeFileSync(`public/uploads/${file.name}`, data)
        fs.unlinkSync(file.path)

        const match = file.name.match(/\d\d\d\d\-\d\d-\d\d/) || (new Date()).toISOString()
        // write JSON
        fs.writeFileSync(`public/uploads/${file.name}.json`, JSON.stringify({
          tags,
          date: (new Date(match)).toISOString()
        }))

        // gen Thumbnail
        genThumbnail(`public/uploads/${file.name}`, `public/uploads/${file.name}.thumb.png`, '300x?')
      })
      .on("aborted", () => {
        reject(res.status(500).send('Aborted'))
      })
      .on("end", () => {
        resolve(res.status(200).send('done'))
      });

    await form.parse(req)
  });
}