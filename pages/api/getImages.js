const fs = require('fs');

export default function handler(req, res) {
    let files = fs.readdirSync('public/uploads/')

    files = files.filter(file => file.endsWith(".json"))

    const data = files.map(filename => { return {
        ...JSON.parse( fs.readFileSync(`public/uploads/${filename}`) ),
        filename: filename.replace(".json", "")
    } })

    res.status(200).json(
        data
    )
}
